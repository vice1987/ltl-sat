#ifndef CLOSURE_TABLE_HPP
#define CLOSURE_TABLE_HPP

struct ClosureRecord
{
	FormulaType type;
	ssize_t left;
	ssize_t right;
	ssize_t fcompl;
};

typedef std::vector<ClosureRecord> ClosureTable;

#endif
