#include "LtlTableau.hpp"

#include <algorithm>
#include <iterator>
#include <functional>
#include <iostream>
#include <stdexcept>

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

LtlTableau::LtlTableau( 
	const ClosureTable& cl, 
	const std::vector<ssize_t>& cl2bs, 
	const std::vector<ssize_t>& bs2cl, 
	ssize_t first_ev_idx,
	ssize_t last_ev_idx, 
	ssize_t false_idx 
) : 
	m_closure(cl), 
	m_cl2bs(cl2bs),
	m_bs2cl(bs2cl),
	m_bs_size(m_bs2cl.size()),
	m_first_ev_idx(first_ev_idx),
	m_last_ev_idx(last_ev_idx),
	m_false_idx(false_idx)
{ 
	this->init();
}

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

void LtlTableau::init()
{
	m_tmp_set = newSet();
	m_uev_set = newSet();
	m_false_set = newSet(); m_false_set->set( m_cl2bs[m_false_idx] );
}

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

bool LtlTableau::initStack( ssize_t idx )
{
	MySetPtr gamma = newSet();
	MySetPtr used  = newSet();
	MySetPtr ev    = newSet();

	m_frame.clear();
	m_frame.push( Frame( gamma, used, ev, Result(-1,nullptr), BranchType::UNDEF ) );

	bool succ = this->insert( *gamma, *used, idx );
	//m_frame.top().gamma_idx = m_last_beta_idx;

	return succ;
}

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

// precondition: m_frame is not empty
void LtlTableau::reduce()
{
	ssize_t idx = static_cast<ssize_t>(m_bs_size)-1; //m_frame_ptr->gamma_idx;

	for( ; idx >= m_first_ev_idx; --idx ) // check whether there is still a beta formula to be reduced
	{
		if( not m_gamma_ptr->test(idx) ) continue; // no formula with index idx in gamma

		switch( m_closure[m_bs2cl[idx]].type )
		{
			// beta formula
			case FormulaType::OR:
			case FormulaType::FINALLY:
			case FormulaType::UNTIL:
				m_frame_ptr->type = BranchType::BETA;
				m_frame_ptr->gamma_idx = idx;
				//m_frame_ptr->ev_idx = (rec.type == FormulaType::OR || m_frame_ptr->ev->test(rec.left)) ? -1 : rec.left;
				return;

			// literal formula
			case FormulaType::VAR:
			case FormulaType::NOT:
			case FormulaType::FALSE:
			case FormulaType::TRUE:
			case FormulaType::NEXT:
				assert(false && "reduce should not encounter an elementary formula");
				return;

			// alpha formula
			case FormulaType::AND:
			case FormulaType::ALWAYS:
			case FormulaType::BEFORE:
				assert(false && "reduce should not encounter an alpha formula");
				return;

			// nexttime formula
			default:
				assert(false);
				return;
		}
	}

	// all beta formulas have been reduced, check for loop or apply nexttime rule

	size_t loop_idx;
	if( this->loop(loop_idx) ) // check whether loop rule can be used
	{
		m_frame_ptr->type = BranchType::TERMINAL;
		m_frame_ptr->result = Result( loop_idx, this->computeLoopUev(loop_idx) );
	}
	else // otherwise, set frame's type as NEXT
	{
		m_frame_ptr->type = BranchType::NEXT;
	}
}

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

bool LtlTableau::loop( size_t &loop_idx )
{
	ssize_t i = static_cast<ssize_t>(m_br.size())-1;

	for( ; i >= 0; --i )
	{ 
		if( *m_gamma_ptr == *(m_br[i].first) )
		{ 
			loop_idx = i; 
			return true; 
		}
	}

	return false;
}

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

MySetPtr LtlTableau::computeLoopUev( size_t loop_idx )
{
	*m_tmp_set = *m_ev_ptr; // initialize with ev
	for( size_t j=loop_idx+1; j < m_br.size(); ++j ) // for each pair br[j], where j>loop_idx
		m_tmp_set->setUnion( *(m_br[j].second) ); // merge br[j].second

	// m_tmp_set now contains all the fulfilled eventualities since the "loop state"

	MySetPtr uev = newSet();

	for( ssize_t idx=0; idx < m_first_ev_idx; ++idx )
	{
		const ClosureRecord& rec = m_closure[ m_bs2cl[idx] ];

		if( m_gamma_ptr->test(idx) && rec.type == FormulaType::NEXT ) // if rec = X(sigma)
		{
			const ClosureRecord& sigma = m_closure[rec.right];

			// test whether sigma's eventuality has been fulfilled
			if( sigma.type == FormulaType::UNTIL || sigma.type == FormulaType::FINALLY )
				if( not this->hasFormula(*m_tmp_set,sigma.left) ) this->insertFormula( *uev, rec.right ); // if unfulfilled, add it to uev
				
				//if( not m_tmp_set->test( m_cl2bs[sigma.left] ) ) // if unfulfilled, add it to uev
				//	uev->set( m_cl2bs[rec.right] );
		}
	}

	return uev;
}

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

void LtlTableau::pushNext()
{
	m_br.emplace_back( m_gamma_ptr.get(), m_ev_ptr.get() );

	MySetPtr newgamma = newSet();
	MySetPtr newused  = newSet();
	MySetPtr newev    = newSet();

	for( ssize_t i=0; i < m_bs_size; ++i )
	{
		const ClosureRecord& rec = m_closure[m_bs2cl[i]];
		if( m_gamma_ptr->test(i) && rec.type == FormulaType::NEXT )
		{
			if( not this->insert( *newgamma, *newused, rec.right ) ) // contraddiction found during new state construction
			{
				m_frame_ptr->type = BranchType::TERMINAL;
				m_frame_ptr->result = Result( m_br.size()+1, m_false_set );
				return;
			}
		}
	}

	if( newgamma->empty() )
	{
		m_frame_ptr->type = BranchType::TERMINAL;
		m_frame_ptr->result = Result( m_br.size()+1, newSet() );
		return;
	}

	m_frame.push( Frame(newgamma,newused,newev,Result(-1,nullptr),BranchType::UNDEF) );
}

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

void LtlTableau::pushBeta()
{
	ssize_t idx = m_frame_ptr->gamma_idx;
	const ClosureRecord& rec = m_closure[ m_bs2cl[idx] ];

	assert( rec.type == FormulaType::OR || rec.type == FormulaType::UNTIL || rec.type == FormulaType::FINALLY );

	MySetPtr newgamma;
	MySetPtr newused; 
	MySetPtr newev;

	if( (m_frame_ptr->result).first == -1 ) // choice point still needs to be visited
	{
		newgamma = newSet(*m_gamma_ptr); newgamma->reset(idx);
		newused  = newSet(*m_used_ptr);
		newev    = newSet(*m_ev_ptr);

		// possibly push beta1 branch
		if( this->insert( *newgamma, *newused, rec.left ) ) // if beta1 insertion is successful
		{
			if(rec.type != FormulaType::OR) this->insertFormula( *newev, rec.left );
			m_frame.push( Frame(newgamma,newused,newev,Result(-1,nullptr),BranchType::UNDEF) );
			return;
		}
		
		// if beta1 leads to a contraddiction, directly add result
		m_frame_ptr->result = Result( m_br.size(), m_false_set );
	}

	newgamma = m_gamma_ptr; newgamma->reset(idx);
	newused  = m_used_ptr;
	newev    = m_ev_ptr;

	// possibly push beta2 branch
	if( this->insert( *newgamma, *newused, rec.right ) ) // && this->insert( *newgamma, *newused, m_closure[rec.left].fcompl ) )
	{
		m_frame.push( Frame(newgamma,newused,newev,Result(-1,nullptr),BranchType::UNDEF) );
		return;
	}

	// if beta2 leads to a contraddiction, directly add result
	m_frame_ptr->type = BranchType::TERMINAL;
	(m_frame_ptr->result).first = std::min( (m_frame_ptr->result).first, static_cast<int>(m_br.size()) );
}

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

// precondition: m_frame has at least 2 elements
void LtlTableau::popFrame()
{
	// get result from top frame and pop it from the "stack"
	Result child_result = std::move(m_frame_ptr->result);
	m_frame.pop();

	assert( child_result.first >= 0 );

	m_frame_ptr = &(m_frame.top());
	m_gamma_ptr = m_frame_ptr->gamma;
	m_used_ptr  = m_frame_ptr->used;
	m_ev_ptr    = m_frame_ptr->ev;

	if( m_frame_ptr->type == BranchType::BETA )
	{
		if( (m_frame_ptr->result).first == -1 ) // add first branch (beta1) result
		{
			m_frame_ptr->result = child_result;

			// possibly restore fulfilled eventualities
			//if( m_frame_ptr->ev_idx != -1 ) m_ev_ptr->reset( m_frame_ptr->ev_idx );
		}
		else // compute result with second branch result
		{
			m_frame_ptr->type = BranchType::TERMINAL; // beta choice-point result has been computed, update frame's status
			this->mergeResults( m_frame_ptr->result, child_result ); // update results with the second branch one	
		}

		return;
	}

	assert( m_frame_ptr->type == BranchType::NEXT );

	// update status
	m_br.pop_back();
	m_frame_ptr->type = BranchType::TERMINAL;
	m_frame_ptr->result = std::move(child_result);
}

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

// merge two results correspondig to a branch
// result is stored in res1
void LtlTableau::mergeResults( Result &res1, const Result &res2 )
{
	int m = static_cast<int>(m_br.size());

	int n1 = res1.first;
	int n2 = res2.first;
	
	res1.first = std::min(n1,n2);

	MySet& uev1 = *(res1.second);
	MySet& uev2 = *(res2.second);

	if( uev1.empty() ) return;
	if( uev2.empty() ){ res1.second = std::move(res2.second); return; }
	
	if( n1 > m and n2 > m )
	{
		res1.second = m_false_set;
		return;
	}

	bool uev2_false = uev2.test( m_cl2bs[m_false_idx] ); //( uev2.count() == 1 and uev2.test(m_closure[m_false_idx].bs_idx) );
	if( (n1 <= m and n2 > m) or uev2_false ) return; // res1.second = uev1;

	bool uev1_false = uev1.test( m_cl2bs[m_false_idx] ); //( uev1.count() == 1 and uev1.test(m_closure[m_false_idx].bs_idx) );
	if( (n1 > m and n2 <= m) or uev1_false )
	{
		res1.second = std::move(res2.second);
		return;
	}

	// intersection between uev1 and uev2
	(res1.second)->setIntersection(uev2);
}

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

bool LtlTableau::isSat( ssize_t formula_idx )
{
	if( not this->initStack(formula_idx) ) return false;

	while( not m_frame.empty() )
	{
		m_frame_ptr = &(m_frame.top());
		m_gamma_ptr = m_frame_ptr->gamma;
		m_used_ptr  = m_frame_ptr->used;
		m_ev_ptr    = m_frame_ptr->ev;

		switch(m_frame_ptr->type)
		{
			case BranchType::UNDEF:
				// update top frame type
				this->reduce();
				break;
				
			case BranchType::TERMINAL:
				// check for eary termination
				if( (m_frame_ptr->result).first >= 0 && (m_frame_ptr->result).second->empty() ) return true;
				if( m_frame.size() == 1 ) return (m_frame_ptr->result).second->empty();
				this->popFrame();
				break;

			case BranchType::BETA:
				this->pushBeta();
				break;

			case BranchType::NEXT:
				this->pushNext();
				break;

			default:
				throw std::logic_error("Branch of undefined type.");
		}
	}

	// empty formula
	return true;
}

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

bool LtlTableau::hasFormula( const MySet& set, ssize_t i )
{
	switch(m_closure[i].type)
	{
		// alpha formulas
		case FormulaType::AND:
		case FormulaType::BEFORE:
		case FormulaType::ALWAYS:
			return this->hasFormula(set,m_closure[i].left) && this->hasFormula(set,m_closure[i].right);

		// beta formulas
		case FormulaType::OR:
		case FormulaType::FINALLY:
		case FormulaType::UNTIL:
			assert( m_cl2bs[i] != -1 );
			return set.test(m_cl2bs[i]) || this->hasFormula(set,m_closure[i].left) || this->hasFormula(set,m_closure[i].right);

		// elementary formulas
		default:
			assert( m_cl2bs[i] != -1 );
			return set.test(m_cl2bs[i]);
	}
}


bool LtlTableau::insert( MySet& gamma, MySet& used, ssize_t i ) // i is an index in the closure table
{
	if( this->insertFormula(gamma,used,i) )
	{
		// check if negation of i-th formula is already in set
		assert( m_closure[i].fcompl != -1 );
		if( not this->hasFormula(used,m_closure[i].fcompl) ) return true;
	}

	return false;
}


void LtlTableau::insertFormula( MySet& evset, ssize_t i ) // i is an index in the closure table
{
	switch( m_closure[i].type )
	{
		case FormulaType::AND:
		case FormulaType::BEFORE:
		case FormulaType::ALWAYS:
			this->insertFormula(evset,m_closure[i].left); 
			this->insertFormula(evset,m_closure[i].right);
			return;

		case FormulaType::FALSE:
			return;

		default:
			//assert( m_cl2bs[i] != -1 );
			// insert formula
			evset.set( m_cl2bs[i] );
			return;
	}
}


bool LtlTableau::insertFormula( MySet& gamma, MySet& used, ssize_t i ) // i is an index in the closure table
{
	switch(m_closure[i].type)
	{
		case FormulaType::AND:
		case FormulaType::BEFORE:
		case FormulaType::ALWAYS:
			return this->insertFormula(gamma,used,m_closure[i].left) && this->insertFormula(gamma,used,m_closure[i].right);

		case FormulaType::FALSE:
			return false;

		default:
			assert( m_cl2bs[i] != -1 );
			assert( m_closure[i].fcompl != -1 );
			if( used.test(m_cl2bs[i]) ) return true; // formula already in set (or already reduced)
			if( m_cl2bs[m_closure[i].fcompl] != -1 && used.test(m_cl2bs[m_closure[i].fcompl]) ) return false; // complement found => contraddiction
			// insert formula
			gamma.set( m_cl2bs[i] );
			used.set( m_cl2bs[i] );
			return true;
	}

	return true;
}

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//
