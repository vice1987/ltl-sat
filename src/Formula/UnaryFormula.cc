#include "UnaryFormula.hpp"

#include <iostream>
#include "Visitor/FormulaVisitor.hpp"

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

FormulaPtr NotFormula::copy() const
{
	return std::make_shared<NotFormula>( right() );
}

void NotFormula::accept( FormulaVisitor &visitor ) 
{ 
	visitor.visit( shared_from_this() ); 
}

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

FormulaPtr NextFormula::copy() const
{
	return std::make_shared<NextFormula>( right() );
}

void NextFormula::accept( FormulaVisitor &visitor ) 
{ 
	visitor.visit( shared_from_this() ); 
}

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

FormulaPtr AlwaysFormula::copy() const
{
	return std::make_shared<AlwaysFormula>( right() );
}

void AlwaysFormula::accept( FormulaVisitor &visitor ) 
{ 
	visitor.visit( shared_from_this() ); 
}

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

FormulaPtr FinallyFormula::copy() const
{
	return std::make_shared<FinallyFormula>( right() );
}

void FinallyFormula::accept( FormulaVisitor &visitor ) 
{ 
	visitor.visit( shared_from_this() ); 
}
