#include "FormulaMap.hpp"

#include <exception>
#include <cassert>

#include <iostream>
#include <iomanip>

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

FormulaMap::val_type FormulaMap::insertPropVar( const std::string& name, bool negated )
{
	assert( name != "true" && name != "false" );

	// search for f in the symbol table
	auto p = m_sym.find( name );

	if( p != m_sym.end() ) // if f already exists, return formula indices
	{
		auto val = p->second;
		auto neg = m_fmap.find( std::make_tuple(FormulaType::NOT, nullptr, val.f) )->second;

		//std::cerr << (negated ? "~" : "") << f->name() << " found: value = (" << (negated ? neg.first : val.first) << "," << (negated ? neg.second : val.second) << ").\n";

		return negated ? neg : val;
	}

	// otherwise, add f and ~f in the closure table in subsequent positions
	FormulaPtr f = m_ffactory.propVarFormula(name);
	FormulaPtr f_neg = m_ffactory.notFormula(f);

	//std::cerr << f->name() << " not found. inserting ";

	ssize_t idx = m_next_idx++;
	ssize_t idx_neg = m_next_idx++;

	m_closure.push_back( { FormulaType::VAR, -1, -1, idx_neg } );
	m_closure.push_back( { FormulaType::NOT, -1, -1, idx } );

	// insert f in the symbol table
	val_type val = { f, idx, f_neg, idx_neg };
	m_sym.emplace( name, val );

	// insert ~f in the formula table
	auto neg_key = std::make_tuple( FormulaType::NOT, nullptr, f );
	val_type neg_val = { f_neg, idx_neg, f, idx };
	m_fmap.emplace( neg_key, neg_val );

	//std::cerr << "~" << f->name() << " value = (" << neg_val.first << "," << neg_val.second << ")." << std::endl;

	return negated ? neg_val : val;
}


FormulaMap::val_type FormulaMap::insertTrue()
{
	// if TRUE constant already exists, return the closure table id
	auto p = m_sym.find("true");
	if( p != m_sym.end() ) return p->second;

	// otherwise insert f and ~f
	FormulaPtr f = m_ffactory.trueFormula();
	FormulaPtr f_neg = m_ffactory.falseFormula();
	ssize_t idx = m_next_idx++;
	ssize_t idx_neg = m_next_idx++;

	m_closure.push_back( { FormulaType::TRUE,  -1, -1, idx_neg } );
	m_closure.push_back( { FormulaType::FALSE, -1, -1, idx } );

	val_type val = { f, idx, f_neg, idx_neg };
	m_sym.emplace( "true", val );

	val_type neg_val = { f_neg, idx_neg, f, idx };
	m_sym.emplace( "false", neg_val );

	return val;
}


FormulaMap::val_type FormulaMap::insertFalse()
{
	// if FALSE already exists, return values from the map
	auto p = m_sym.find("false");
	if( p != m_sym.end() ) return p->second;

	// otherwise insert f and ~f
	FormulaPtr f = m_ffactory.falseFormula();
	FormulaPtr f_neg = m_ffactory.trueFormula();
	ssize_t idx = m_next_idx++;
	ssize_t idx_neg = m_next_idx++;

	m_closure.push_back( { FormulaType::FALSE, -1, -1, idx_neg } );
	m_closure.push_back( { FormulaType::TRUE,  -1, -1, idx } );

	val_type val = { f, idx, f_neg, idx_neg };
	m_sym.emplace( "false", val );

	val_type neg_val = { f_neg, idx_neg, f, idx };
	m_sym.emplace( "true", neg_val );

	return val;
}

//***************************************************************************//

FormulaMap::val_type FormulaMap::insertAnd( const val_type& left, const val_type& right )
{
	auto key1 = std::make_tuple( FormulaType::AND, left.f, right.f );
	auto it1  = m_fmap.find(key1);

	if( it1 == m_fmap.end() ) // key1 not found
	{
		auto key2 = std::make_tuple( FormulaType::AND, right.f, left.f );
		auto it2 = m_fmap.find(key2);

		if( it2 == m_fmap.end() ) // key2 also not found, insert formula
		{
			// add a record in the closure table
			FormulaPtr f = m_ffactory.andFormula(left.f,right.f);
			ssize_t idx = m_next_idx++;
			FormulaPtr f_neg = m_ffactory.orFormula(left.f_neg,right.f_neg);
			ssize_t idx_neg = m_next_idx++;

			m_closure.push_back( { FormulaType::AND, left.idx, right.idx, idx_neg } );
			m_closure.push_back( { FormulaType::OR, left.idx_neg, right.idx_neg, idx } );

			// add f in the lookup table
			val_type val = { f, idx, f_neg, idx_neg };
			m_fmap.emplace( key1, val );

			// add ~f in the lookup table
			auto neg_key = std::make_tuple( FormulaType::OR, left.f_neg, right.f_neg );
			val_type neg_val = { f_neg, idx_neg, f, idx };
			m_fmap.emplace( neg_key, neg_val );

			return val;
		}
			
		return it2->second;
	}

	return it1->second;
}



FormulaMap::val_type FormulaMap::insertOr( const val_type& left, const val_type& right )
{
	auto key1 = std::make_tuple( FormulaType::OR, left.f, right.f );
	auto it1  = m_fmap.find(key1);

	if( it1 == m_fmap.end() ) // key1 not found
	{
		auto key2 = std::make_tuple( FormulaType::OR, right.f, left.f );
		auto it2 = m_fmap.find(key2);

		if( it2 == m_fmap.end() ) // key2 also not found, insert formula
		{
			// add a record in the closure table
			FormulaPtr f = m_ffactory.orFormula(left.f,right.f);
			ssize_t idx = m_next_idx++;
			FormulaPtr f_neg = m_ffactory.andFormula(left.f_neg,right.f_neg);
			ssize_t idx_neg = m_next_idx++;

			m_closure.push_back( { FormulaType::OR, left.idx, right.idx, idx_neg } );
			m_closure.push_back( { FormulaType::AND, left.idx_neg, right.idx_neg, idx } );

			// add f in the lookup table
			val_type val = { f, idx, f_neg, idx_neg };
			m_fmap.emplace( key1, val );

			// add ~f in the lookup table
			auto neg_key = std::make_tuple( FormulaType::AND, left.f_neg, right.f_neg );
			val_type neg_val = { f_neg, idx_neg, f, idx };
			m_fmap.emplace( neg_key, neg_val );

			return val;
		}
			
		return it2->second;
	}

	return it1->second;
}



FormulaMap::val_type FormulaMap::insertUntil( const val_type& left, const val_type& right )
{
	auto key1 = std::make_tuple( FormulaType::UNTIL, left.f, right.f );
	auto it1  = m_fmap.find(key1);

	if( it1 == m_fmap.end() ) // key1 not found
	{
		// add a record in the closure table
		FormulaPtr f = m_ffactory.untilFormula(left.f,right.f); // f = left UNTIL right
		ssize_t idx = m_next_idx++;
		FormulaPtr f_neg = m_ffactory.beforeFormula(left.f_neg,right.f); // not(f) = not(left) BEFORE right
		ssize_t idx_neg = m_next_idx++;

		// insert beta2(f) in the closure

		FormulaPtr ntf = m_ffactory.nextFormula(f); // X(f)
		ssize_t nt_idx = m_next_idx++;
		FormulaPtr ntf_neg = m_ffactory.nextFormula(f_neg); // not(X(f)) = X(not(f)) 
		ssize_t nt_idx_neg = m_next_idx++;

		FormulaPtr andf = m_ffactory.andFormula( left.f, ntf ); // left AND X(f) = beta2
		ssize_t and_idx = m_next_idx++;
		FormulaPtr andf_neg = m_ffactory.orFormula( left.f_neg, ntf_neg ); // not(left) OR not(X(f)) = not(beta2)
		ssize_t and_idx_neg = m_next_idx++;

		m_closure.push_back( { FormulaType::UNTIL, right.idx, and_idx, idx_neg } );
		m_closure.push_back( { FormulaType::BEFORE, right.idx_neg, and_idx_neg, idx } );
		m_closure.push_back( { FormulaType::NEXT, -1, idx, nt_idx_neg } );
		m_closure.push_back( { FormulaType::NEXT, -1, idx_neg, nt_idx } );
		m_closure.push_back( { FormulaType::AND, left.idx, nt_idx, and_idx_neg } );
		m_closure.push_back( { FormulaType::OR, left.idx_neg, nt_idx_neg, and_idx } );

		// insert f and ~f in lookup table

		val_type val = { f, idx, f_neg, idx_neg };
		m_fmap.emplace( key1, val );
		auto neg_key = std::make_tuple( FormulaType::BEFORE, left.f_neg, right.f );
		val_type neg_val = { f_neg, idx_neg, f, idx };
		m_fmap.emplace( neg_key, neg_val );

		// insert X(f) and X(~f) in lookup table

		auto ntf_key = std::make_tuple( FormulaType::NEXT, nullptr, f );
		val_type ntf_val = { ntf, nt_idx, ntf_neg, nt_idx_neg };
		m_fmap.emplace( ntf_key, ntf_val );
		auto ntf_neg_key = std::make_tuple( FormulaType::NEXT, nullptr, f_neg );
		val_type ntf_neg_val = { ntf_neg, nt_idx_neg, ntf, nt_idx };
		m_fmap.emplace( ntf_neg_key, ntf_neg_val );

		// insert beta2(f) and ~beta2(f) in lookup table

		auto andf_key = std::make_tuple( FormulaType::AND, left.f, ntf );
		val_type andf_val = { andf, and_idx, andf_neg, and_idx_neg };
		m_fmap.emplace( andf_key, andf_val );
		auto andf_neg_key = std::make_tuple( FormulaType::OR, left.f_neg, ntf_neg );
		val_type andf_neg_val = { andf_neg, and_idx_neg, andf, and_idx };
		m_fmap.emplace( andf_neg_key, andf_neg_val );

		return val;
	}

	return it1->second;
}


FormulaMap::val_type FormulaMap::insertBefore( const val_type& left, const val_type& right )
{
	auto key1 = std::make_tuple( FormulaType::BEFORE, left.f, right.f );
	auto it1  = m_fmap.find(key1);

	if( it1 == m_fmap.end() ) // key1 not found
	{
		// add a record in the closure table
		FormulaPtr f = m_ffactory.beforeFormula(left.f,right.f); // f = left BEFORE right
		ssize_t idx = m_next_idx++;
		FormulaPtr f_neg = m_ffactory.untilFormula(left.f_neg,right.f); // not(f) = not(left) UNTIL right
		ssize_t idx_neg = m_next_idx++;

		// insert beta2(f) in the closure

		FormulaPtr ntf = m_ffactory.nextFormula(f); // X(f)
		ssize_t nt_idx = m_next_idx++;
		FormulaPtr ntf_neg = m_ffactory.nextFormula(f_neg); // not(X(f)) = X(not(f)) 
		ssize_t nt_idx_neg = m_next_idx++;

		FormulaPtr orf = m_ffactory.orFormula( left.f, ntf ); // left OR X(f) = alpha2
		ssize_t or_idx = m_next_idx++;
		FormulaPtr orf_neg = m_ffactory.andFormula( left.f_neg, ntf_neg ); // not(left) AND not(X(f)) = not(alpha2)
		ssize_t or_idx_neg = m_next_idx++;

		m_closure.push_back( { FormulaType::BEFORE, right.idx_neg, or_idx, idx_neg } );
		m_closure.push_back( { FormulaType::UNTIL,  right.idx, or_idx_neg, idx } );

		m_closure.push_back( { FormulaType::NEXT, -1, idx, nt_idx_neg } );
		m_closure.push_back( { FormulaType::NEXT, -1, idx_neg, nt_idx } );

		m_closure.push_back( { FormulaType::OR, left.idx, nt_idx, or_idx_neg } );
		m_closure.push_back( { FormulaType::AND, left.idx_neg, nt_idx_neg, or_idx } );

		// insert f and ~f in lookup table

		val_type val = { f, idx, f_neg, idx_neg };
		m_fmap.emplace( key1, val );
		auto neg_key = std::make_tuple( FormulaType::UNTIL, left.f_neg, right.f );
		val_type neg_val = { f_neg, idx_neg, f, idx };
		m_fmap.emplace( neg_key, neg_val );

		// insert X(f) and X(~f) in lookup table

		auto ntf_key = std::make_tuple( FormulaType::NEXT, nullptr, f );
		val_type ntf_val = { ntf, nt_idx, ntf_neg, nt_idx_neg };
		m_fmap.emplace( ntf_key, ntf_val );
		auto ntf_neg_key = std::make_tuple( FormulaType::NEXT, nullptr, f_neg );
		val_type ntf_neg_val = { ntf_neg, nt_idx_neg, ntf, nt_idx };
		m_fmap.emplace( ntf_neg_key, ntf_neg_val );

		// insert alpha2(f) and ~alpha2(f) in lookup table

		auto orf_key = std::make_tuple( FormulaType::OR, left.f, ntf );
		val_type orf_val = { orf, or_idx, orf_neg, or_idx_neg };
		m_fmap.emplace( orf_key, orf_val );
		auto orf_neg_key = std::make_tuple( FormulaType::AND, left.f_neg, ntf_neg );
		val_type orf_neg_val = { orf_neg, or_idx_neg, orf, or_idx };
		m_fmap.emplace( orf_neg_key, orf_neg_val );

		return val;
	}

	return it1->second;
}


//***************************************************************************//


FormulaMap::val_type FormulaMap::insertNext( const val_type& right )
{
	auto key1 = std::make_tuple( FormulaType::NEXT, nullptr, right.f );
	auto it1  = m_fmap.find(key1);

	if( it1 == m_fmap.end() ) // key1 not found
	{
		// add a record in the closure table
		FormulaPtr f = m_ffactory.nextFormula(right.f); // f = left BEFORE right
		ssize_t idx = m_next_idx++;
		FormulaPtr f_neg = m_ffactory.nextFormula(right.f_neg); // not(f) = not(left) UNTIL right
		ssize_t idx_neg = m_next_idx++;

		m_closure.push_back( { FormulaType::NEXT, -1, right.idx,     idx_neg } );
		m_closure.push_back( { FormulaType::NEXT, -1, right.idx_neg, idx } );

		// add f in the lookup table
		val_type val = { f, idx, f_neg, idx_neg };
		m_fmap.emplace( key1, val );

		// add ~f in the lookup table
		auto neg_key = std::make_tuple( FormulaType::NEXT, nullptr, right.f_neg );
		val_type neg_val = { f_neg, idx_neg, f, idx };
		m_fmap.emplace( neg_key, neg_val );

		return val;
	}

	return it1->second;
}


FormulaMap::val_type FormulaMap::insertAlways( const val_type& right )
{
	auto key1 = std::make_tuple( FormulaType::ALWAYS, nullptr, right.f );
	auto it1  = m_fmap.find(key1);

	if( it1 == m_fmap.end() ) // key1 not found
	{
		FormulaPtr f = m_ffactory.alwaysFormula(right.f); // f = left BEFORE right
		ssize_t idx = m_next_idx++;
		FormulaPtr f_neg = m_ffactory.finallyFormula(right.f_neg); // not(f) = not(left) UNTIL right
		ssize_t idx_neg = m_next_idx++;

		FormulaPtr ntf = m_ffactory.nextFormula(f); // X(f)
		ssize_t nt_idx = m_next_idx++;
		FormulaPtr ntf_neg = m_ffactory.nextFormula(f_neg); // not(X(f)) = X(not(f)) 
		ssize_t nt_idx_neg = m_next_idx++;

		m_closure.push_back( { FormulaType::ALWAYS,  right.idx, nt_idx, idx_neg } );
		m_closure.push_back( { FormulaType::FINALLY, right.idx_neg, nt_idx_neg, idx } );
		m_closure.push_back( { FormulaType::NEXT, -1, idx, nt_idx_neg } );
		m_closure.push_back( { FormulaType::NEXT, -1, idx_neg, nt_idx } );

		// add f in the lookup table
		val_type val = { f, idx, f_neg, idx_neg };
		m_fmap.emplace( key1, val );

		// add ~f in the lookup table
		auto neg_key = std::make_tuple( FormulaType::FINALLY, nullptr, right.f_neg );
		val_type neg_val = { f_neg, idx_neg, f, idx };
		m_fmap.emplace( neg_key, neg_val );

		// insert X(f) and X(~f) in lookup table

		auto ntf_key = std::make_tuple( FormulaType::NEXT, nullptr, f );
		val_type ntf_val = { ntf, nt_idx, ntf_neg, nt_idx_neg };
		m_fmap.emplace( ntf_key, ntf_val );
		auto ntf_neg_key = std::make_tuple( FormulaType::NEXT, nullptr, f_neg );
		val_type ntf_neg_val = { ntf_neg, nt_idx_neg, ntf, nt_idx };
		m_fmap.emplace( ntf_neg_key, ntf_neg_val );

		return val;
	}

	return it1->second;
}


FormulaMap::val_type FormulaMap::insertFinally( const val_type& right )
{
	auto key1 = std::make_tuple( FormulaType::FINALLY, nullptr, right.f );
	auto it1  = m_fmap.find(key1);

	if( it1 == m_fmap.end() ) // key1 not found
	{
		FormulaPtr f = m_ffactory.finallyFormula(right.f); // f = left BEFORE right
		ssize_t idx = m_next_idx++;
		FormulaPtr f_neg = m_ffactory.alwaysFormula(right.f_neg); // not(f) = not(left) UNTIL right
		ssize_t idx_neg = m_next_idx++;

		FormulaPtr ntf = m_ffactory.nextFormula(f); // X(f)
		ssize_t nt_idx = m_next_idx++;
		FormulaPtr ntf_neg = m_ffactory.nextFormula(f_neg); // not(X(f)) = X(not(f)) 
		ssize_t nt_idx_neg = m_next_idx++;

		m_closure.push_back( { FormulaType::FINALLY, right.idx, nt_idx, idx_neg } );
		m_closure.push_back( { FormulaType::ALWAYS,  right.idx_neg, nt_idx_neg, idx } );
		m_closure.push_back( { FormulaType::NEXT, -1, idx, nt_idx_neg } );
		m_closure.push_back( { FormulaType::NEXT, -1, idx_neg, nt_idx } );

		// add f in the lookup table
		val_type val = { f, idx, f_neg, idx_neg };
		m_fmap.emplace( key1, val );

		// add ~f in the lookup table
		auto neg_key = std::make_tuple( FormulaType::ALWAYS, nullptr, right.f_neg );
		val_type neg_val = { f_neg, idx_neg, f, idx };
		m_fmap.emplace( neg_key, neg_val );

		// insert X(f) and X(~f) in lookup table

		auto ntf_key = std::make_tuple( FormulaType::NEXT, nullptr, f );
		val_type ntf_val = { ntf, nt_idx, ntf_neg, nt_idx_neg };
		m_fmap.emplace( ntf_key, ntf_val );
		auto ntf_neg_key = std::make_tuple( FormulaType::NEXT, nullptr, f_neg );
		val_type ntf_neg_val = { ntf_neg, nt_idx_neg, ntf, nt_idx };
		m_fmap.emplace( ntf_neg_key, ntf_neg_val );

		return val;
	}

	return it1->second;
}
