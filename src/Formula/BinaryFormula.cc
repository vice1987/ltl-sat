#include "BinaryFormula.hpp"

#include <iostream>
#include "Visitor/FormulaVisitor.hpp"

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

FormulaPtr AndFormula::copy() const
{
	return std::make_shared<AndFormula>( left()->copy(), right()->copy() );
}

void AndFormula::accept( FormulaVisitor &visitor ) 
{ 
	visitor.visit( shared_from_this() ); 
}

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

FormulaPtr OrFormula::copy() const
{
	return std::make_shared<OrFormula>( left()->copy(), right()->copy() );
}

void OrFormula::accept( FormulaVisitor &visitor ) 
{ 
	visitor.visit( shared_from_this() ); 
}

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

FormulaPtr UntilFormula::copy() const
{
	return std::make_shared<UntilFormula>( left()->copy(), right()->copy() );
}

void UntilFormula::accept( FormulaVisitor &visitor ) 
{ 
	visitor.visit( shared_from_this() ); 
}

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

FormulaPtr BeforeFormula::copy() const
{
	return std::make_shared<BeforeFormula>( left()->copy(), right()->copy() );
}

void BeforeFormula::accept( FormulaVisitor &visitor ) 
{ 
	visitor.visit( shared_from_this() ); 
}
