#include "LiteralFormula.hpp"

#include <iostream>
#include "Visitor/FormulaPrinter.hpp"

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

FormulaPtr PropVarFormula::copy() const
{
	return std::make_shared<PropVarFormula>( name() );
}


void PropVarFormula::accept( FormulaVisitor &visitor )
{
	visitor.visit( shared_from_this() );
}

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

FormulaPtr TrueFormula::copy() const
{
	return std::make_shared<TrueFormula>();
}


void TrueFormula::accept( FormulaVisitor &visitor )
{
	visitor.visit( shared_from_this() );
}

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

FormulaPtr FalseFormula::copy() const
{
	return std::make_shared<FalseFormula>();
}


void FalseFormula::accept( FormulaVisitor &visitor )
{
	visitor.visit( shared_from_this() );
}
