#ifndef UNARY_FORMULA_HPP
#define UNARY_FORMULA_HPP

#include "Formula.hpp"

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

class UnaryFormula;
typedef std::shared_ptr<UnaryFormula> UnaryFormulaPtr;

class NotFormula;
typedef std::shared_ptr<NotFormula> NotFormulaPtr;

class NextFormula;
typedef	std::shared_ptr<NextFormula> NextFormulaPtr;

class AlwaysFormula;
typedef std::shared_ptr<AlwaysFormula> AlwaysFormulaPtr;

class FinallyFormula;
typedef std::shared_ptr<FinallyFormula> FinallyFormulaPtr;

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

class UnaryFormula : public Formula
{

private:

	FormulaPtr m_right;

public:
	
	UnaryFormula( FormulaType type, FormulaPtr right ) : 
		Formula( type, right->len() + 1 ), 
		m_right(right) 
	{ }
	
	const FormulaPtr right() const { return m_right; }

};

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

class NotFormula : public UnaryFormula, public std::enable_shared_from_this<NotFormula>
{

public:

	NotFormula( FormulaPtr right ) : UnaryFormula( FormulaType::NOT, right ) { }

	FormulaPtr copy() const;
	void accept( FormulaVisitor &visitor );

};

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

class NextFormula : public UnaryFormula, public std::enable_shared_from_this<NextFormula>
{

public:

	NextFormula( FormulaPtr right ) : UnaryFormula( FormulaType::NEXT, right ) { }

	FormulaPtr copy() const;
	void accept( FormulaVisitor &visitor );
	
};

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

class AlwaysFormula : public UnaryFormula, public std::enable_shared_from_this<AlwaysFormula>
{

public:

	AlwaysFormula( FormulaPtr right ) : UnaryFormula( FormulaType::ALWAYS, right ) { }

	FormulaPtr copy() const;
	void accept( FormulaVisitor &visitor );
	
};

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

class FinallyFormula : public UnaryFormula, public std::enable_shared_from_this<FinallyFormula>
{

public:

	FinallyFormula( FormulaPtr right ) : UnaryFormula( FormulaType::FINALLY, right ) { }

	FormulaPtr copy() const;
	void accept( FormulaVisitor &visitor );
	
};

#endif //UNARY_FORMULA_HPP
