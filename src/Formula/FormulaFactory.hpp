#ifndef FORMULA_FACTORY_HPP
#define FORMULA_FACTORY_HPP

#include <memory>

#include "BinaryFormula.hpp"
#include "UnaryFormula.hpp"
#include "LiteralFormula.hpp"

class FormulaFactory
{

private:

	static std::unique_ptr<FormulaFactory> s_instance;
	FormulaFactory() { }

public:

	static const FormulaFactory& instance();

	// atomic formulas
	PropVarFormulaPtr propVarFormula( const std::string& name ) const { return std::make_shared<PropVarFormula>(name); }
	TrueFormulaPtr trueFormula() const { return std::make_shared<TrueFormula>(); }
	FalseFormulaPtr falseFormula() const { return std::make_shared<FalseFormula>(); }

	// unary formulas
	NotFormulaPtr notFormula( FormulaPtr right ) const { return std::make_shared<NotFormula>(right); }
	NextFormulaPtr nextFormula( FormulaPtr right ) const { return std::make_shared<NextFormula>(right); }
	AlwaysFormulaPtr alwaysFormula( FormulaPtr right ) const { return std::make_shared<AlwaysFormula>(right); }
	FinallyFormulaPtr finallyFormula( FormulaPtr right ) const { return std::make_shared<FinallyFormula>(right); }

	// binary formulas
	AndFormulaPtr andFormula( FormulaPtr left, FormulaPtr right ) const { return std::make_shared<AndFormula>(left,right); }
	OrFormulaPtr orFormula( FormulaPtr left, FormulaPtr right ) const { return std::make_shared<OrFormula>(left,right); }
	UntilFormulaPtr untilFormula( FormulaPtr left, FormulaPtr right ) const { return std::make_shared<UntilFormula>(left,right); }
	BeforeFormulaPtr beforeFormula( FormulaPtr left, FormulaPtr right ) const { return std::make_shared<BeforeFormula>(left,right); }

};

#endif
