#ifndef FORMULA_MAP_HPP
#define FORMULA_MAP_HPP

#include <vector>
#include <tuple>
#include <map>

//#include <google/sparse_hash_map>
//using google::sparse_hash_map;

#include "FormulaFactory.hpp"
#include "ClosureTable.hpp"

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

class FormulaMap
{

public: // private definitions and members
	
	typedef std::tuple< FormulaType, FormulaPtr, FormulaPtr > key_type;
	
	//typedef std::tuple< FormulaPtr, ssize_t, FormulaPtr, ssize_t > val_type;

	class val_type
	{

	public:

		FormulaPtr f;
		ssize_t idx;
		FormulaPtr f_neg;
		ssize_t idx_neg;

	public:

		bool operator==( const val_type& o ) const 
		{ 
			return f == o.f && idx == o.idx && f_neg == o.f_neg && idx_neg == o.idx_neg;
		}

		bool operator!=( const val_type& o ) const
		{
			return f != o.f || idx != o.idx || f_neg != o.f_neg || idx_neg != o.idx_neg;
		}
	};

private:

	std::map< std::string, val_type > m_sym; // symbol lookup table
	std::map< key_type, val_type > m_fmap;   // formula lookup table

	ssize_t m_next_idx;

	ClosureTable m_closure;
	const FormulaFactory& m_ffactory;

public:

	FormulaMap() : m_next_idx(0), m_ffactory(FormulaFactory::instance()) { }

	void clear() 
	{ 
		m_sym.clear(); 
		m_fmap.clear();
		m_next_idx = 0;
		m_closure.clear();
	}

	/*val_type insert( PropVarFormulaPtr f, bool negated = false );
	val_type insert( TrueFormulaPtr f );
	val_type insert( FalseFormulaPtr f );
	val_type insert( UnaryFormulaPtr f,  ssize_t right );
	val_type insert( AndFormulaPtr f,    ssize_t left, ssize_t right );
	val_type insert( OrFormulaPtr f,     ssize_t left, ssize_t right );
	val_type insert( UntilFormulaPtr f,  ssize_t left, ssize_t right );
	val_type insert( BeforeFormulaPtr f, ssize_t left, ssize_t right );*/

	val_type insertPropVar( const std::string& name, bool negated = false );
	val_type insertTrue();
	val_type insertFalse();

	val_type insertAnd( const val_type& left, const val_type& right );
	val_type insertOr( const val_type& left, const val_type& right );
	val_type insertBefore( const val_type& left, const val_type& right );
	val_type insertUntil( const val_type& left, const val_type& right );

	val_type insertNext( const val_type& right );
	val_type insertFinally( const val_type& right );
	val_type insertAlways( const val_type& right );

	ClosureTable& getClosure() { return m_closure; }
	const ClosureTable& getClosure() const { return m_closure; }

};

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

#endif // FORMULA_HASH_TABLE_HPP
