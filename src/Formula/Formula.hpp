#ifndef FORMULA_HPP
#define FORMULA_HPP

#include <map>
#include <memory>

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

class Formula;
class FormulaVisitor;

typedef std::shared_ptr<Formula> FormulaPtr;

enum class FormulaType : std::uint8_t 
{ 
	// binary operators */
	OR, AND, UNTIL, BEFORE,
	// unary operators */
	NOT, NEXT, ALWAYS, FINALLY,
	// propositional variables/constants */
	VAR, TRUE, FALSE
};

class Formula
{

private:

	FormulaType m_type;
	size_t m_len;

public:

	Formula( FormulaType type, size_t len = 1 ) : m_type(type), m_len(len) { }

	FormulaType type() const { return m_type; }
	size_t len() const { return m_len; }

public:

	virtual FormulaPtr copy() const = 0; // deep copy method
	virtual void accept( FormulaVisitor& ) = 0; // accept printer visitor
};

#endif // FORMULA_HPP
