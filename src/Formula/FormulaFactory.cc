#include "FormulaFactory.hpp"

std::unique_ptr<FormulaFactory> FormulaFactory::s_instance = nullptr;

const FormulaFactory& FormulaFactory::instance()
{
	if( !s_instance ) s_instance = std::unique_ptr<FormulaFactory>( new FormulaFactory() );

	return *s_instance;
}


