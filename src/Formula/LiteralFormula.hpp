#ifndef LITERAL_FORMULA_HPP
#define LITERAL_FORMULA_HPP

#include "Formula.hpp"

#include <string>

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

class PropVarFormula;
typedef std::shared_ptr<PropVarFormula> PropVarFormulaPtr;

class TrueFormula;
typedef std::shared_ptr<TrueFormula> TrueFormulaPtr;

class FalseFormula;
typedef std::shared_ptr<FalseFormula> FalseFormulaPtr;

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

class PropVarFormula : public Formula, public std::enable_shared_from_this<PropVarFormula>
{

private:

	std::string m_name;

public:
	
	PropVarFormula( const std::string &name ) : 
		Formula( FormulaType::VAR, 1 ), 
		m_name(name)
	{ }

	std::string name() const { return m_name; }

public:

	virtual FormulaPtr copy() const;
	virtual void accept( FormulaVisitor &visitor );
};

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

class TrueFormula : public Formula, public std::enable_shared_from_this<TrueFormula>
{

public:
	
	TrueFormula() : Formula( FormulaType::TRUE, 1 ) { }

public:

	virtual FormulaPtr copy() const;
	virtual void accept( FormulaVisitor &visitor );
};

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

class FalseFormula : public Formula, public std::enable_shared_from_this<FalseFormula>
{

public:
	
	FalseFormula() : Formula( FormulaType::FALSE, 1 ) { }

public:

	virtual FormulaPtr copy() const;
	virtual void accept( FormulaVisitor &visitor );
};

#endif //LITERAL_FORMULA_HPP
