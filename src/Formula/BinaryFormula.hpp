#ifndef BINARY_FORMULA_HPP
#define BINARY_FORMULA_HPP

#include "Formula.hpp"

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

class BinaryFormula;
typedef	std::shared_ptr<BinaryFormula> BinaryFormulaPtr;

class AndFormula;
typedef std::shared_ptr<AndFormula> AndFormulaPtr;

class OrFormula;
typedef std::shared_ptr<OrFormula> OrFormulaPtr;

class UntilFormula;
typedef std::shared_ptr<UntilFormula> UntilFormulaPtr;

class BeforeFormula;
typedef std::shared_ptr<BeforeFormula> BeforeFormulaPtr;

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

class BinaryFormula : public Formula
{

private:

	FormulaPtr m_left;
	FormulaPtr m_right;

public:
	
	BinaryFormula( FormulaType type, FormulaPtr left, FormulaPtr right ) : 
		Formula( type, left->len() + right->len() + 1 ), 
		m_left(left), 
		m_right(right)
	{ }

	const FormulaPtr left() const { return m_left; }
	const FormulaPtr right() const { return m_right; }

};

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

class AndFormula : public BinaryFormula, public std::enable_shared_from_this<AndFormula>
{

public:

	AndFormula( FormulaPtr left, FormulaPtr right ) : BinaryFormula( FormulaType::AND, left, right ) { }

	FormulaPtr copy() const;
	void accept( FormulaVisitor &visitor );

};

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

class OrFormula : public BinaryFormula, public std::enable_shared_from_this<OrFormula>
{

public:

	OrFormula( FormulaPtr left, FormulaPtr right ) : BinaryFormula( FormulaType::OR, left, right ) { }

	FormulaPtr copy() const;
	void accept( FormulaVisitor &visitor );

};

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

class UntilFormula : public BinaryFormula, public std::enable_shared_from_this<UntilFormula>
{

public:

	UntilFormula( FormulaPtr left, FormulaPtr right ) : BinaryFormula( FormulaType::UNTIL, left, right ) { }

	FormulaPtr copy() const;
	void accept( FormulaVisitor &visitor );

};

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

class BeforeFormula : public BinaryFormula, public std::enable_shared_from_this<BeforeFormula>
{

public:

	BeforeFormula( FormulaPtr left, FormulaPtr right ) : BinaryFormula( FormulaType::BEFORE, left, right ) { }

	FormulaPtr copy() const;
	void accept( FormulaVisitor &visitor );

};

#endif //BINARY_FORMULA_HPP
