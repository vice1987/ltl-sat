#ifndef TOKEN_HPP
#define TOKEN_HPP

#include <iosfwd>
#include <cstdint>
#include <string>
#include <map>

//typedef int32_t token_t;

enum class TokenType : std::uint8_t 
{ 
	// binary operators */
	IFF, IMPL, OR, AND, UNTIL, BEFORE,
	// unary operators */
	NOT, NEXT, ALWAYS, FINALLY,
	// variables/constants */
	VAR, TRUE, FALSE,
	// other tokens */
	LB, RB, UNKNOWN
};


struct TokenProperty
{
	std::string str;
	int  prec;
	bool is_unary;
};


class Token
{

public:

	static const std::map<TokenType,TokenProperty> Property;

private:

	TokenType m_type;
	int m_line;
	int m_column;
	std::string m_text;

public:

	Token();

	TokenType type() const;
	int line() const;
	int column() const;
	const std::string& text() const;

	void set( TokenType type, int line, int col, const char *text );

	friend std::ostream& operator <<( std::ostream &os, const Token &t );
};

#endif
