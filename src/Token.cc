#include "Token.hpp"

#include <iostream>


const std::map<TokenType,TokenProperty> Token::Property = 
{
	// binary
	{ TokenType::IFF,     { "IFF", 10, false } },
	{ TokenType::IMPL,    { "IMPL", 10, false } },
	{ TokenType::OR,      { "OR", 11, false } },
	{ TokenType::AND,     { "AND", 12, false } },
	{ TokenType::UNTIL,   { "UNTIL", 13, false } },
	{ TokenType::BEFORE,  { "BEFORE", 13, false } },
	// unary
	{ TokenType::NOT,     { "NOT", 20, true } },
	{ TokenType::NEXT,    { "NEXT", 21, true } },
	{ TokenType::ALWAYS,  { "ALWAYS", 22, true } },
	{ TokenType::FINALLY, { "FINALLY", 23, true } },
	// literals/constants
	{ TokenType::VAR,     { "VAR", 30, false } },
	{ TokenType::TRUE,    { "TRUE", 30, false } },
	{ TokenType::FALSE,   { "FALSE", 30, false } },
	// others
	{ TokenType::LB,      { "LB", 0, false } },
	{ TokenType::RB,      { "RB", 0, false } },
	{ TokenType::UNKNOWN, { "UNKNOWN", 0, false } }
};


Token::Token()
{
	this->set( TokenType::UNKNOWN, -1, -1, "" );
}

void Token::set( TokenType type, int line, int col, const char *text )
{
	m_type = type;
	m_line = line;
	m_column = col;
	m_text = text;
}

TokenType Token::type() const 
{ 
	return m_type;
}


int Token::line() const
{
	return m_line;
}


int Token::column() const
{
	return m_column;
}


const std::string& Token::text() const
{
	return m_text;
}


std::ostream& operator <<( std::ostream &os, const Token &t )
{
	os << "[" << Token::Property.at(t.m_type).str << ";(" << t.m_line << ":" 
	   << t.m_column << ");\"" << t.m_text << "\"]";

    return os;
}
