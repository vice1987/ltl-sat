#ifndef PARSER_HPP
#define PARSER_HPP

#include <stack>

#include "Scanner.hpp"
#include "Formula/FormulaFactory.hpp"

class Parser
{

private:

	std::stack<TokenType>  m_op;
	std::stack<FormulaPtr> m_out;
	const FormulaFactory&  m_ffactory;

	/*************************************/

	void push_unary( TokenType op );
	void push_binary( TokenType op );

	void update_formula( TokenType op );
	void abort( const char *msg, int line=0, int col=0 );

public:

	Parser() : m_ffactory(FormulaFactory::instance()) { }
	
	FormulaPtr parse( Scanner &scanner );

};

#endif //PARSER_HPP
