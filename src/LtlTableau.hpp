#ifndef LTL_TABLEAU_HPP
#define LTL_TABLEAU_HPP

#include <set>
#include <list>
#include <stack>
#include <cassert>

#include "Frame.hpp"
#include "MyStack.hpp"

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

class LtlTableau
{

private: 

	typedef std::pair<MySet*,MySet*> BrPair;
	typedef std::vector<BrPair> Branch;

private:

	const ClosureTable& m_closure;       // closure table
	const std::vector<ssize_t>& m_cl2bs; // map a formula in the closure to the index in a bitset (undefined for alphas)
	const std::vector<ssize_t>& m_bs2cl; // map a formula in a bitset (which does not contain alphas) to the closure table

	const size_t  m_bs_size;      // size of a bitset
	const ssize_t m_false_idx;    // index of the FALSE constant formula in the closure table
	const ssize_t m_first_ev_idx; // index of the first "beta" formula that might appear in a bitset (previous indices refer to elementary formulas)
	const ssize_t m_last_ev_idx;

	MySetPtr    m_false_set;
	//MySetPtr    m_neg_mask;
	
	// temporary pre-allocated objects
	MySetPtr    m_tmp_set;
	MySetPtr    m_uev_set;

	Branch m_br; // branch history: (gamma,ev) pairs
	MyStack m_frame; // stack
	
	// temporary values of the top frame members
	Frame* m_frame_ptr;
	MySetPtr m_gamma_ptr;
	MySetPtr m_used_ptr;
	MySetPtr m_ev_ptr;

	inline MySetPtr newSet() const { return std::make_shared<MySet>(m_bs_size); }
	inline MySetPtr newSet( const MySet& set ) const { return std::make_shared<MySet>(set); }
	//inline MySetPtr newUevSet() const { return std::make_shared<MySet>(m_last_ev_idx-m_first_ev_idx+1); }

private:

	void init();
	bool initStack( ssize_t idx );
	void reduce();
	
	bool loop( size_t &loop_idx );
	MySetPtr computeLoopUev( size_t loop_idx );
	
	// push the nexttime pre-state in the stack
	void pushNext();
	// push a pre-state which correspond to the unexplored beta1 or beta2 branch
	void pushBeta();
	// result in the current branch has been computed, propagate it "upwards" in the tableau
	void popFrame();
	// merge two results in a choicepoint
	void mergeResults( Result& res1, const Result& res2 );

	// check whether a formula is in a set
	bool hasFormula( const MySet& set, ssize_t i );
	// insert a formula in two sets checking for complement
	bool insert( MySet& gamma, MySet& used, ssize_t i );
	// insert a formula in two sets
	bool insertFormula( MySet& gamma, MySet& used, ssize_t i );
	// insert a formula in a single set without checking for complements
	void insertFormula( MySet& evset, ssize_t i );

public:

	LtlTableau( 
		const ClosureTable& cl, 
		const std::vector<ssize_t>& cl2bs, 
		const std::vector<ssize_t>& bs2cl, 
		ssize_t first_ev_idx,
		ssize_t last_ev_idx, 
		ssize_t false_idx 
	);
	
	bool isSat( ssize_t formula_idx );

};

#endif //LTL_TABLEAU_HPP
