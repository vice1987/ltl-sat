/*
 * File:   function_stats.hpp
 * Author: Riccardo Vicedomini
 *
 * Created on November 14, 2013, 5:16 PM
 */

#ifndef TIMER_HPP
#define	TIMER_HPP

#include <chrono>
#include <string>
#include <sstream>

class Timer
{

private:

	typedef std::chrono::high_resolution_clock clock;

	clock::time_point m_start;

	std::chrono::milliseconds elapsedMs( const clock::time_point& cur )
	{
		return std::chrono::duration_cast< std::chrono::milliseconds >(cur-m_start);
	}

public:

	Timer() : m_start(clock::now()) { }

	clock::time_point restart() 
	{ 
		m_start = clock::now(); 
		return m_start;
	}

	std::string getElapsedString()
	{
		std::stringstream out;
		auto ms = elapsedMs(clock::now());

		if( ms.count() < 1000 )
		{
			out << ms.count() << "ms";
		}
		else
		{
			auto seconds = std::chrono::duration_cast< std::chrono::seconds >(ms);
			auto h = seconds.count() / 3600;
			auto m = (seconds.count() % 3600) / 60;
			auto s = (seconds.count() % 3600) % 60;

			if( h > 0 ) out << h << "h";
			if( m > 0 ) out << m << "m";
			out << s << "s";
		}

		return out.str();
	}
};

#endif	/* TIMER_HPP */
