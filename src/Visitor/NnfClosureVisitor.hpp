#ifndef NNF_CLOSURE_VISITOR_HPP
#define NNF_CLOSURE_VISITOR_HPP

#include "Formula/FormulaMap.hpp"
#include "ClosureTable.hpp"
#include "FormulaVisitor.hpp"

class NnfClosureVisitor : public FormulaVisitor
{

private:

	const FormulaFactory&  m_ffactory;

	bool    m_negated;

	FormulaMap m_fht;
	FormulaMap::val_type m_formula;

	std::vector<ssize_t> m_bs2cl;
	std::vector<ssize_t> m_cl2bs;

	ssize_t m_or_idx;
	ssize_t m_ev_idx;
	ssize_t m_nt_idx;
	ssize_t m_at_idx;
	ssize_t m_false_idx;

	//ssize_t m_nt_first;
	//ssize_t m_nt_last;
	//ssize_t m_ev_first;
	//ssize_t m_ev_last;
	ssize_t m_first_ev_idx;
	ssize_t m_last_ev_idx;

public:

	NnfClosureVisitor() : m_ffactory(FormulaFactory::instance()) 
	{ 
		this->init(); 
	}

	inline void init() 
	{ 
		m_negated = false;
		m_fht.clear();
		m_formula = { nullptr, -1, nullptr, -1 };
		m_false_idx = m_fht.insertFalse().idx;
	}

	inline ssize_t getFirstEvIdx() { return m_first_ev_idx; }
	inline ssize_t getLastEvIdx() { return m_last_ev_idx; }
	inline ssize_t getFalseIdx() { return m_false_idx; }
	inline ssize_t getFormulaIdx() { return m_formula.idx; }
	inline FormulaPtr getNnfFormula() { return m_formula.f; }

	inline const ClosureTable& getClosure() const { return m_fht.getClosure(); }
	inline const std::vector<ssize_t>& getBsToCl() const { return m_bs2cl; }
	inline const std::vector<ssize_t>& getClToBs() const { return m_cl2bs; }

	void computeBsIndices();

	void visit( PropVarFormulaPtr f );
	void visit( TrueFormulaPtr f );
	void visit( FalseFormulaPtr f );

	void visit( AndFormulaPtr f );
	void visit( OrFormulaPtr f );
	void visit( UntilFormulaPtr f );
	void visit( BeforeFormulaPtr f );

	void visit( NotFormulaPtr f );
	void visit( NextFormulaPtr f );
	void visit( AlwaysFormulaPtr f );
	void visit( FinallyFormulaPtr f );
	
};

#endif // NNF_CLOSURE_VISITOR_HPP
