#include "NnfClosureVisitor.hpp"

#include <cassert>
#include <exception>
//#include <iostream>
//#include <iomanip>

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

void NnfClosureVisitor::visit( PropVarFormulaPtr f )
{
	//Formula *neg_f = new NotFormula(f);
	//m_formula = m_negated ? m_fht.insert(neg_f) : m_fht.insert(f);

	//std::cerr << std::hex << "processing " << (m_negated ? "~" : "") << f->name() << ", id=" << f << std::endl;

	m_formula = m_fht.insertPropVar( f->name(), m_negated ); // m_fht.insert(f,m_negated);

	//std::cerr << "returned value => (" << m_formula.first << "," << m_formula.second << ")" << std::endl;
}

void NnfClosureVisitor::visit( TrueFormulaPtr f )
{
	//std::cerr << std::hex << "processing " << (m_negated ? "~" : "") << "\"true\", id=" << f << std::endl;

	m_formula = m_negated ? m_fht.insertFalse() : m_fht.insertTrue();

	//std::cerr << "returned value => (" << m_formula.first << "," << m_formula.second << ")" << std::endl;
}

void NnfClosureVisitor::visit( FalseFormulaPtr f )
{
	//std::cerr << std::hex << "processing " << (m_negated ? "~" : "") << "\"false\", id=" << f << std::endl;

	m_formula = m_negated ? m_fht.insertTrue() : m_fht.insertFalse();

	//std::cerr << "returned value => (" << m_formula.first << "," << m_formula.second << ")" << std::endl;
}

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

// Binary Formulas

void NnfClosureVisitor::visit( AndFormulaPtr f )
{
	//std::cerr << std::hex << "processing " 
	//	<< (m_negated ? "~" : "") << "AND(" << f->left() << "," << f->right() << ") id=" << f << std::endl;

	bool negate = m_negated;
	f->left()->accept(*this);
	auto left = m_formula;

	m_negated = negate;
	f->right()->accept(*this);
	auto right = m_formula;

	//std::cerr << std::hex << "found left=(" << left.first << "," << left.second << ")"
	//	<< " right=(" << right.first << "," << right.second << ")\n";

	if( left != right )
	{
		m_formula = negate 
			? m_fht.insertOr(left,right)   //m_fht.insert( m_ffactory.orFormula(left.first,right.first), left.second, right.second ) 
			: m_fht.insertAnd(left,right); //m_fht.insert( m_ffactory.andFormula(left.first,right.first), left.second, right.second );
	}

	//std::cerr << "returned value => " << (negate ? "OR" : "AND") 
	//	<< "(" << m_formula.first << "," << m_formula.second << ")" << std::endl;
}

void NnfClosureVisitor::visit( OrFormulaPtr f )
{
	//std::cerr << std::hex << "processing " 
	//	<< (m_negated ? "~" : "") << "OR(" << f->left() << "," << f->right() << ") id=" << f << std::endl;

	bool negate = m_negated;
	f->left()->accept(*this);
	auto left = m_formula;

	m_negated = negate;
	f->right()->accept(*this);
	auto right = m_formula;

	//std::cerr << std::hex << "found left=(" << left.first << "," << left.second << ")"
	//	<< " right=(" << right.first << "," << right.second << ")\n";

	if( left != right )
	{
		m_formula = negate 
			? m_fht.insertAnd(left,right) // m_fht.insert( m_ffactory.andFormula(left.first,right.first), left.second, right.second ) 
			: m_fht.insertOr(left,right); // m_fht.insert( m_ffactory.orFormula(left.first,right.first), left.second, right.second );
	}

	//std::cerr << "returned value => " << (negate ? "AND" : "OR") 
	//	<< "(" << m_formula.first << "," << m_formula.second << ")" << std::endl;
}

void NnfClosureVisitor::visit( UntilFormulaPtr f )
{
	//std::cerr << std::hex << "processing " 
	//	<< (m_negated ? "~" : "") << "UNTIL(" << f->left() << "," << f->right() << ") id=" << f << std::endl;

	bool negate = m_negated;
	f->left()->accept(*this);
	auto left = m_formula;

	m_negated = false; //negate ? true : false; // for Before formulas right is forced to be the complement (i.e., ~(right))
	f->right()->accept(*this);
	auto right = m_formula;

	//std::cerr << std::hex << "found left=(" << left.first << "," << left.second << ")"
	//	<< " right=(" << right.first << "," << right.second << ")\n";

	if( left != right )
	{
		m_formula = negate 
			? m_fht.insertBefore(left,right)  // m_fht.insert( m_ffactory.beforeFormula(left.first,right.first), left.second, right.second ) 
			: m_fht.insertUntil(left,right);  // m_fht.insert( m_ffactory.untilFormula(left.first,right.first), left.second, right.second );
	}

	//std::cerr << "returned value => " << (negate ? "BEFORE" : "UNTIL") 
	//	<< "(" << m_formula.first << "," << m_formula.second << ")" << std::endl;
}

void NnfClosureVisitor::visit( BeforeFormulaPtr f )
{
	//std::cerr << std::hex << "processing " 
	//	<< (m_negated ? "~" : "") << "BEFORE(" << f->left() << "," << f->right() << ") id=" << f << std::endl;

	bool negate = m_negated;
	f->left()->accept(*this);
	auto left = m_formula;

	m_negated = false;  //negate ? false : true; // for Before formulas right is forced to be the complement (i.e., ~(right))
	f->right()->accept(*this);
	auto right = m_formula;

	//std::cerr << std::hex << "found left=(" << left.first << "," << left.second << ")"
	//	<< " right=(" << right.first << "," << right.second << ")\n";

	m_formula = negate 
		? m_fht.insertUntil(left,right)   // m_fht.insert( m_ffactory.untilFormula(left.first,right.first), left.second, right.second ) 
		: m_fht.insertBefore(left,right); // m_fht.insert( m_ffactory.beforeFormula(left.first,right.first), left.second, right.second );

	//std::cerr << "returned value => " << (negate ? "UNTIL" : "BEFORE") 
	//	<< "(" << m_formula.first << "," << m_formula.second << ")" << std::endl;
}

// Unary Formulas

void NnfClosureVisitor::visit( NotFormulaPtr f )
{
	m_negated = not m_negated;
	f->right()->accept(*this);
}

void NnfClosureVisitor::visit( NextFormulaPtr f )
{
	//std::cerr << std::hex << "processing " 
	//	<< (m_negated ? "~" : "") << "NEXT(" << f->right() << ") id=" << f << std::endl;

	f->right()->accept(*this);
	auto right = m_formula;

	//std::cerr << std::hex << "found right=(" << right.first << "," << right.second << ")\n";

	m_formula = m_fht.insertNext(right); // m_fht.insert( m_ffactory.nextFormula(right.first), right.second );

	//std::cerr << "returned value => " 
	//	<< "NEXT(" << m_formula.first << "," << m_formula.second << ")" << std::endl;
}

void NnfClosureVisitor::visit( AlwaysFormulaPtr f )
{
	//std::cerr << std::hex << "processing " 
	//	<< (m_negated ? "~" : "") << "ALWAYS(" << f->right() << ") id=" << f << std::endl;

	bool negate = m_negated;
	f->right()->accept(*this);
	auto right = m_formula;

	//std::cerr << std::hex << "found right=(" << right.first << "," << right.second << ")\n";

	m_formula = negate 
		? m_fht.insertFinally(right) // m_fht.insert( m_ffactory.finallyFormula(right.first), right.second ) 
		: m_fht.insertAlways(right); // m_fht.insert( m_ffactory.alwaysFormula(right.first), right.second );

	//std::cerr << "returned value => " << (negate ? "FINALLY" : "ALWAYS") 
	//	<< "(" << m_formula.first << "," << m_formula.second << ")" << std::endl;
}

void NnfClosureVisitor::visit( FinallyFormulaPtr f )
{
	//std::cerr << std::hex << "processing " 
	//	<< (m_negated ? "~" : "") << "FINALLY(" << f->right() << ") id=" << f << std::endl;

	bool negate = m_negated;
	f->right()->accept(*this);
	auto right = m_formula;

	//std::cerr << std::hex << "found right=(" << right.first << "," << right.second << ")\n";

	m_formula = negate 
		? m_fht.insertAlways(right)   // m_fht.insert( m_ffactory.alwaysFormula(right.first), right.second )
		: m_fht.insertFinally(right); // m_fht.insert( m_ffactory.finallyFormula(right.first), right.second );

	//std::cerr << "returned value => " << (negate ? "ALWAYS" : "FINALLY") 
	//	<< "(" << m_formula.first << "," << m_formula.second << ")" << std::endl;
}

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

void NnfClosureVisitor::computeBsIndices()
{
	if( m_formula.idx < 0 ) return; // TODO: empty formula, throw exception?

	ClosureTable& ct = m_fht.getClosure();

	ssize_t at_num = 0;     // number of atoms (positive/negative literals, TRUE, FALSE)
	ssize_t nt_num = 0;     // number of nexttime formulas
	ssize_t alphas_num = 0; // number of alpha formulas (And, Before, Globally)
	ssize_t ev_num = 0;     // number of eventuality formulas (Until, Finally)
	ssize_t or_num = 0;     // number of Or formulas

	// count formulas
	for( size_t i=0; i < ct.size(); ++i )
	{
		switch( ct[i].type )
		{
			case FormulaType::VAR:
			case FormulaType::NOT:
			case FormulaType::TRUE:
			case FormulaType::FALSE: 
				at_num++; 
				break;

			case FormulaType::NEXT:
				nt_num++;
				break;

			case FormulaType::AND:
			case FormulaType::BEFORE:
			case FormulaType::ALWAYS:
				alphas_num++;
				break;

			case FormulaType::OR:
				or_num++;
				break;

			case FormulaType::UNTIL:
			case FormulaType::FINALLY:
				ev_num++;
				break;
		}
	}

	m_at_idx = 0;
	m_nt_idx = at_num;
	m_ev_idx = m_nt_idx + nt_num;
	m_or_idx = m_ev_idx + ev_num;

	m_first_ev_idx = m_ev_idx;
	m_last_ev_idx  = m_ev_idx+ev_num-1;

	size_t bs_size = at_num + nt_num + or_num + ev_num;

	m_bs2cl.resize( bs_size,   -1 );
	m_cl2bs.resize( ct.size(), -1 );

	//assert( m_bs2cl.size() == m_cl2bs.size() );

	for( size_t i = 0; i < ct.size(); ++i )
	{
		switch( ct[i].type )
		{
			case FormulaType::FALSE:
			case FormulaType::TRUE:
			case FormulaType::VAR:
			case FormulaType::NOT: 
				m_bs2cl.at(m_at_idx) = i;
				m_cl2bs.at(i) = m_at_idx++;
				break;

			case FormulaType::NEXT:
				m_bs2cl.at(m_nt_idx) = i;
				m_cl2bs.at(i) = m_nt_idx++;
				break;

			case FormulaType::AND:
			case FormulaType::BEFORE:
			case FormulaType::ALWAYS:
				break;

			case FormulaType::OR:
				m_bs2cl.at(m_or_idx) = i;
				m_cl2bs.at(i) = m_or_idx++;
				break;

			case FormulaType::UNTIL:
			case FormulaType::FINALLY:
				m_bs2cl.at(m_ev_idx) = i;
				m_cl2bs.at(i) = m_ev_idx++;
				break;
		}
	}
}

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

