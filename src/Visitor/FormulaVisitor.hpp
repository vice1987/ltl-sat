/* visitor class which prints a formula */

#ifndef FORMULA_VISITOR_HPP
#define FORMULA_VISITOR_HPP

#include "Formula/LiteralFormula.hpp"
#include "Formula/UnaryFormula.hpp"
#include "Formula/BinaryFormula.hpp"

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//
class FormulaVisitor
{

public:

	virtual void visit( PropVarFormulaPtr f );
	virtual void visit( TrueFormulaPtr f );
	virtual void visit( FalseFormulaPtr f );

	virtual void visit( AndFormulaPtr f );
	virtual void visit( OrFormulaPtr f );
	virtual void visit( UntilFormulaPtr f );
	virtual void visit( BeforeFormulaPtr f );

	virtual void visit( NotFormulaPtr f );
	virtual void visit( NextFormulaPtr f );
	virtual void visit( AlwaysFormulaPtr f );
	virtual void visit( FinallyFormulaPtr f );

protected:

	         FormulaVisitor() { }
	virtual ~FormulaVisitor() { }

};

#endif // FORMULA_VISITOR_HPP
