/* visitor class which prints a formula */

#ifndef FORMULA_PRINTER_HPP
#define FORMULA_PRINTER_HPP

#include <iostream>

#include "FormulaVisitor.hpp"

class FormulaPrinter : public FormulaVisitor
{

private:

	std::ostream &m_os;

public:

	FormulaPrinter( std::ostream &os = std::cout ) : m_os(os) { }

	void visit( PropVarFormulaPtr f );
	void visit( TrueFormulaPtr f );
	void visit( FalseFormulaPtr f );

	void visit( AndFormulaPtr f );
	void visit( OrFormulaPtr f );
	void visit( UntilFormulaPtr f );
	void visit( BeforeFormulaPtr f );

	void visit( NotFormulaPtr f );
	void visit( NextFormulaPtr f );
	void visit( AlwaysFormulaPtr f );
	void visit( FinallyFormulaPtr f );
	
};

#endif // FORMULA_PRINTER_HPP
