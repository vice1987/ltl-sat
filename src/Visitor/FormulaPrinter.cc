#include "FormulaPrinter.hpp"

#include <exception>

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

void FormulaPrinter::visit( PropVarFormulaPtr f )
{
	m_os << f->name();
}

void FormulaPrinter::visit( TrueFormulaPtr f )
{
	m_os << "true";
}

void FormulaPrinter::visit( FalseFormulaPtr f )
{
	m_os << "false";
}

// Binary Formulas

void FormulaPrinter::visit( AndFormulaPtr f )
{
	m_os << "(";
	f->left()->accept(*this) ;
	m_os << " & ";
	f->right()->accept(*this);
	m_os << ")";
}

void FormulaPrinter::visit( OrFormulaPtr f )
{
	m_os << "(";
	f->left()->accept(*this) ;
	m_os << " | ";
	f->right()->accept(*this);
	m_os << ")";
}

void FormulaPrinter::visit( UntilFormulaPtr f )
{
	m_os << "(";
	f->left()->accept(*this) ;
	m_os << " U ";
	f->right()->accept(*this);
	m_os << ")";
}

void FormulaPrinter::visit( BeforeFormulaPtr f )
{
	m_os << "(";
	f->left()->accept(*this) ;
	m_os << " B ";
	f->right()->accept(*this);
	m_os << ")";
}

// Unary Formulas

void FormulaPrinter::visit( NotFormulaPtr f )
{
	m_os << "~"; f->right()->accept(*this);
}

void FormulaPrinter::visit( NextFormulaPtr f )
{
	m_os << "X"; f->right()->accept(*this);
}

void FormulaPrinter::visit( AlwaysFormulaPtr f )
{
	m_os << "G"; f->right()->accept(*this);
}

void FormulaPrinter::visit( FinallyFormulaPtr f )
{
	m_os << "F"; f->right()->accept(*this);
}

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//
