#include "FormulaVisitor.hpp"

#include <exception>

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//

void FormulaVisitor::visit( PropVarFormulaPtr f )
{
	throw std::invalid_argument("Visitor cannot visit a Literal Formulas");
}

void FormulaVisitor::visit( TrueFormulaPtr f )
{
	throw std::invalid_argument("Visitor cannot visit a Literal Formulas");
}

void FormulaVisitor::visit( FalseFormulaPtr f )
{
	throw std::invalid_argument("Visitor cannot visit a Literal Formulas");
}

// Binary Formulas

void FormulaVisitor::visit( AndFormulaPtr f )
{
	throw std::invalid_argument("Visitor cannot visit an And Formulas");
}

void FormulaVisitor::visit( OrFormulaPtr f )
{
	throw std::invalid_argument("Visitor cannot visit an Or Formulas");
}

void FormulaVisitor::visit( UntilFormulaPtr f )
{
	throw std::invalid_argument("Visitor cannot visit an Until Formulas");
}

void FormulaVisitor::visit( BeforeFormulaPtr f )
{
	throw std::invalid_argument("Visitor cannot visit a Before Formulas");
}

// Unary Formulas

void FormulaVisitor::visit( NotFormulaPtr f )
{
	throw std::invalid_argument("Visitor cannot visit a Not Formulas");
}

void FormulaVisitor::visit( NextFormulaPtr f )
{
	throw std::invalid_argument("Visitor cannot visit a Next Formulas");
}

void FormulaVisitor::visit( AlwaysFormulaPtr f )
{
	throw std::invalid_argument("Visitor cannot visit an Always Formulas");
}

void FormulaVisitor::visit( FinallyFormulaPtr f )
{
	throw std::invalid_argument("Visitor cannot visit a Finally Formulas");
}

//***************************************************************************//
//***************************************************************************//
//***************************************************************************//
