#include <iostream>

#include "Timer.hpp"
#include "Scanner.hpp"
#include "Parser.hpp"
#include "Formula/FormulaMap.hpp"
#include "Visitor/FormulaPrinter.hpp"
#include "Visitor/NnfClosureVisitor.hpp"

#include "LtlTableau.hpp"

int main(int argc, char *argv[]) 
{
	Timer timer;

	if( argc != 2 )
	{
		std::cout << "Usage: " << argv[0] << " <formula.ltl>" << std::endl;
		return EXIT_FAILURE;
	}
	
	FormulaPtr f(nullptr);
	Scanner scanner(argv[1]);
	
	try
	{
		Parser parser;
		f = parser.parse(scanner);
	}
	catch( std::string &err_msg )
	{
		std::cerr << err_msg << std::endl;
		return EXIT_FAILURE;
	}

	if( f == nullptr )
	{
		std::cerr << "Formula is empty." << std::endl;
		return EXIT_FAILURE;
	}

	std::cout << "Formula: " << std::endl;
	FormulaPrinter printer; f->accept(printer);
	std::cout << "\nLen: " << f->len() << std::endl;

	NnfClosureVisitor closureVisitor;
	f->accept(closureVisitor);

	std::cout << "\nNNF Formula: " << std::endl;
	closureVisitor.getNnfFormula()->accept( printer );
	std::cout << "\nLen: " << closureVisitor.getNnfFormula()->len() << std::endl;

	// build a tableau with a given closure
	closureVisitor.computeBsIndices();
	LtlTableau tableau( closureVisitor.getClosure(), closureVisitor.getClToBs(), closureVisitor.getBsToCl(), closureVisitor.getFirstEvIdx(), closureVisitor.getLastEvIdx(), closureVisitor.getFalseIdx() );
	bool sat = tableau.isSat( closureVisitor.getFormulaIdx() );

	std::cout << "\nSatisfiable: " << (sat ? "yes" : "no") << std::endl;
	std::cout << "Time: " << timer.getElapsedString() << std::endl;
	
	return EXIT_SUCCESS;
}
