#include "Scanner.hpp"

Scanner::Scanner() 
{
	this->init();
}

Scanner::Scanner( const char *path )
{
	this->open(path);
}

Scanner::Scanner( const std::string &path )
{
	this->open(path);
}

Scanner::~Scanner()
{
	this->close();
}

void Scanner::open( const std::string &path )
{
	this->open( path.c_str() );
}

void Scanner::init()
{
	m_line = 1;
	m_column = 1;
	m_text = "";
}

void Scanner::open( const char *path )
{
	if( m_ifs.is_open() ) this->close();

	this->init();
	m_ifs.open(path);
}


void Scanner::close()
{
	m_ifs.close();
}


const std::string& Scanner::text() const
{
	return m_text;
}

int Scanner::line() const
{
	return m_line;
}

int Scanner::column() const
{
	return m_column;
}

const Token& Scanner::token() const
{
	return m_token;
}


bool Scanner::next()
{
	m_text.clear();

	char c = m_ifs.peek();

	while( m_ifs.good() )
	{
		m_ifs.get(c); // read next character
		
		if( isblank(c) ){ m_column++; c = m_ifs.peek(); continue; } // skip whitespaces
		if( c == '\n' ){ m_line++; m_column = 1; c = m_ifs.peek(); continue; } // skip newlines

		m_text.push_back(c);

		// handle one character tokens
		switch(c)
		{
			case '(': 
				m_token.set( TokenType::LB, m_line, m_column++, m_text.c_str() );
				return true;
			case ')':
				m_token.set( TokenType::RB, m_line, m_column++, m_text.c_str() );
				return true;
			case '&':
				m_token.set( TokenType::AND, m_line, m_column++, m_text.c_str() );
				return true;
			case '|':
				m_token.set( TokenType::OR, m_line, m_column++, m_text.c_str() );
				return true;
			case 'U':
				m_token.set( TokenType::UNTIL, m_line, m_column++, m_text.c_str() );
				return true;
			case 'B':
				m_token.set( TokenType::BEFORE, m_line, m_column++, m_text.c_str() );
				return true;
			case 'G':
				m_token.set( TokenType::ALWAYS, m_line, m_column++, m_text.c_str() );
				return true;
			case 'F':
				m_token.set( TokenType::FINALLY, m_line, m_column++, m_text.c_str() );
				return true;
			case '~':
			case '!':
				m_token.set( TokenType::NOT, m_line, m_column++, m_text.c_str() );
				return true;
			case 'X':
				m_token.set( TokenType::NEXT, m_line, m_column++, m_text.c_str() );
				return true;
		}

		// handle logical implication token
		if( c == '=' or c == '-' )
		{
			c = m_ifs.peek();

			if( c == '>' ) // valid token
			{
				m_ifs.ignore(1);
				m_text.push_back(c);
				m_token.set( TokenType::IMPL, m_line, m_column, m_text.c_str() );
				m_column += 2;

				return true;
			}

			m_token.set( TokenType::UNKNOWN, m_line, m_column, m_text.c_str() );
			m_column++;
			
			return true;
		}

		// handle iff
		if( c == '<' )
		{
			c = m_ifs.peek();

			if( c == '=' or c == '-' )
			{
				m_ifs.ignore(1);
				m_text.push_back(c);

				c = m_ifs.peek();

				if( c == '>' ) // valid token
				{
					m_ifs.ignore(1);
					m_text.push_back(c);
					m_token.set( TokenType::IFF, m_line, m_column, m_text.c_str() );
					m_column += 3;

					return true;
				}
			}

			m_token.set( TokenType::UNKNOWN, m_line, m_column, m_text.c_str() );
			m_column++;
			
			return true;
		}

		// handle identifiers/constants (lower case alphanumeric strings and ":_")
		if( (c >= 'a' && c <= 'z') || c == ':' || c == '_' )
		{
			c = m_ifs.peek();

			while( m_ifs.good() && (islower(c) || isdigit(c) || c == ':' || c == '_') )
			{ 
				m_text.push_back(c);
				m_ifs.ignore(1); 
				c = m_ifs.peek();
			}

			if( m_text == "true" ) 
				m_token.set( TokenType::TRUE, m_line, m_column, m_text.c_str() );
			else if( m_text == "false" )
				m_token.set( TokenType::FALSE, m_line, m_column, m_text.c_str() );
			else
				m_token.set( TokenType::VAR, m_line, m_column, m_text.c_str() );
			
			m_column += m_text.size();
			return true;
		}

		// UNKNOWN TOKEN
		m_token.set( TokenType::UNKNOWN, m_line, m_column, m_text.c_str() );
		m_column++;

		return true;
	}

	return false;
}


