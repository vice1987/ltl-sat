#include "Parser.hpp"

#include <iostream>
#include <sstream>


FormulaPtr Parser::parse( Scanner &scanner )
{
	while( scanner.next() )
	{
		const Token &token = scanner.token();
		TokenType type = token.type();

		if( type == TokenType::VAR )
		{
			m_out.push( m_ffactory.propVarFormula(token.text()) );
			continue;
		}

		if( type == TokenType::TRUE )
		{
			m_out.push( m_ffactory.trueFormula() );
			continue;
		}

		if( type == TokenType::FALSE )
		{
			m_out.push( m_ffactory.falseFormula() );
			continue;
		}

		// left bracket
		if( type == TokenType::LB )
		{
			m_op.push(type);
			continue;
		}

		// right bracket
		if( type == TokenType::RB )
		{
			while( not m_op.empty() && m_op.top() != TokenType::LB )
			{
				this->update_formula(m_op.top());
				m_op.pop();
			}

			if( m_op.empty() ) 
				this->abort( "mismatched parenthesis", token.line(), token.column() );

			m_op.pop(); // pop left bracket

			while( not m_op.empty() && Token::Property.at(m_op.top()).is_unary )
			{
				this->update_formula(m_op.top());
				m_op.pop();
			}

			continue;
		}

		// unknown token
		if( type == TokenType::UNKNOWN )
			this->abort( "unknown token", token.line(), token.column() );

		// remaining tokens (i.e., operators)
		// update formula while there are higher (or equal) priority operators
		while( not m_op.empty() )
		{
			TokenType op2 = m_op.top();

			if( (not Token::Property.at(type).is_unary) and (Token::Property.at(type).prec <= Token::Property.at(op2).prec) )
			{
				m_op.pop();
				this->update_formula(op2);
			}
			else
			{
				break;
			}
		}

		m_op.push(type); // push current operator in the stack
	}

	// NO MORE TOKENS, finish to build the formula with remaining operators

	while( not m_op.empty() )
	{
		TokenType op = m_op.top(); m_op.pop();

		if( op == TokenType::LB || op == TokenType::RB ) 
			this->abort("mismatched parenthesis");

		this->update_formula(op);
	}

	if( m_out.size() > 1 ) this->abort("missing operand");

	return ( m_out.size() == 0 ? nullptr : m_out.top() );
}


void Parser::abort( const char *msg, int line, int col )
{
	std::stringstream ss;

	// print error message
	ss << "[error] " << msg;
	if( line > 0 && col > 0 ) ss << " at " << line << ":" << col;
	ss << std::endl;

	while( not m_out.empty() ) m_out.pop();

	// throw "string" exception
	throw std::string(ss.str());
}


void Parser::update_formula( TokenType op )
{
	if( Token::Property.at(op).is_unary )
	{
		if( m_out.size() < 1 ) this->abort("missing operand");
		this->push_unary(op);
	}
	else // binary operator
	{
		if( m_out.size() < 2 ) this->abort("missing operands");
		this->push_binary(op);
	}
}


void Parser::push_unary( TokenType op )
{
	FormulaPtr f = m_out.top(); m_out.pop();

	switch(op)
	{
		case TokenType::NOT:     m_out.push( m_ffactory.notFormula(f) ); break;
		case TokenType::NEXT:    m_out.push( m_ffactory.nextFormula(f) ); break;
		case TokenType::ALWAYS:  m_out.push( m_ffactory.alwaysFormula(f) ); break;
		case TokenType::FINALLY: m_out.push( m_ffactory.finallyFormula(f) ); break;

		default: this->abort("unknown unary operator"); break;
	}
}


void Parser::push_binary( TokenType op )
{
	FormulaPtr right = m_out.top(); m_out.pop();
	FormulaPtr left  = m_out.top(); m_out.pop();

	switch(op)
	{
		case TokenType::AND:     m_out.push( m_ffactory.andFormula(left,right) ); break;
		case TokenType::OR:      m_out.push( m_ffactory.orFormula(left,right) ); break;
		case TokenType::UNTIL:   m_out.push( m_ffactory.untilFormula(left,right) ); break;
		case TokenType::BEFORE:  m_out.push( m_ffactory.beforeFormula(left,right) ); break;

		case TokenType::IMPL:    m_out.push( m_ffactory.orFormula(m_ffactory.notFormula(left),right) ); break;

		case TokenType::IFF:    
			m_out.push( 
				m_ffactory.andFormula( 
					m_ffactory.orFormula( m_ffactory.notFormula(left), right ),
					m_ffactory.orFormula( m_ffactory.notFormula(right), left )
				)
			);
			break;

		default: this->abort("unknown binary operator"); break;
	}
}
