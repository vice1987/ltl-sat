#ifndef FRAME_HPP
#define FRAME_HPP

#include "MySet.hpp"

typedef std::pair<int,MySetPtr> Result;

enum class BranchType : std::uint8_t
{
	UNDEF,
	BETA,
	NEXT,
	TERMINAL
};

struct Frame
{
	MySetPtr  gamma;
	MySetPtr  used;
	MySetPtr  ev;
	Result result;
	
	BranchType type;
	ssize_t gamma_idx; // next formula to consider
	//ssize_t ev_idx;    // eventuality possibly added to the beta1 branch

	Frame( MySetPtr _gamma, MySetPtr _used, MySetPtr _ev, Result _result, BranchType _type, ssize_t _gamma_idx = -1 ) : //, ssize_t _ev_idx ) :
		gamma(_gamma),
		used(_used),
		ev(_ev),
		result(_result),
		type(_type),
		gamma_idx(_gamma_idx)
		//ev_idx(_ev_idx)
	{ }

	Frame() : 
		gamma(nullptr),
		used(nullptr),
		ev(nullptr),
		result(-1,nullptr),
		type(BranchType::UNDEF),
		gamma_idx(-1)
		//ev_idx(-1)
	{ }

	Frame( Frame&& ) = default;
	Frame( const Frame& ) = default;
};

#endif // FRAME_HPP
