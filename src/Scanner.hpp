#ifndef SCANNER_HPP
#define SCANNER_HPP

#include <fstream>
#include <string>

#include "Token.hpp"

class Scanner
{

private:

	int m_line;
	int m_column;
	std::string m_text;

	std::ifstream m_ifs;

	Token m_token;

	void init();

public:

	Scanner();
	Scanner( const char *path );
	Scanner( const std::string &path );
	~Scanner();

	void open( const char *path );
	void open( const std::string &path );
	void close();

	const std::string& text() const;
	int line() const;
	int column() const;

	const Token& token() const;

	bool next();
};


#endif
