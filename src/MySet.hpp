#ifndef MY_SET_HPP
#define MY_SET_HPP

#include <vector>
#include <limits>

#include "Formula/FormulaMap.hpp"

class MySet;
typedef std::shared_ptr<MySet> MySetPtr;

class MySet
{

private:

	typedef uint64_t block_t;
	static constexpr size_t bits_per_block = std::numeric_limits<block_t>::digits;

	std::vector<block_t> m_set;
	size_t m_size; // max number of bits that can be set

	// block_t should not be larger than 128 bits
	inline size_t count( size_t pos ) const
	{
		size_t c = 0;
		block_t v = m_set[pos];
		
		v = v - ((v >> 1) & (block_t)~(block_t)0/3);
		v = (v & (block_t)~(block_t)0/15*3) + ((v >> 2) & (block_t)~(block_t)0/15*3);
		v = (v + (v >> 4)) & (block_t)~(block_t)0/255*15;
		c = (block_t)(v * ((block_t)~(block_t)0/255)) >> (sizeof(block_t) - 1) * std::numeric_limits<unsigned char>::digits;

		return c;
	}

	// given a bit offset, return the position in the internal vector
	inline size_t vec_index( size_t off ) const
	{
		return off / bits_per_block;
	}

	// return a block_t where the only bit set is the one at position (off % bits_per_block)
	inline block_t mask( size_t off ) const
	{
		return block_t(1) << (off%bits_per_block);
	}

public:

	MySet( size_t size = 0 ) : 
		m_size(size), 
		m_set( size == 0 ? 0 : ((size-1)/bits_per_block)+1, block_t(0) )
	{ }
	
	MySet( const MySet& other ) = default;
	MySet( MySet&& other ) = default;

	MySet& operator=( const MySet& other ) = default;
	MySet& operator=( MySet&& other ) = default;

	inline size_t size() const 
	{ 
		return m_size;  
	}
	
	inline size_t count() const 
	{ 
		size_t count = 0;
		for( size_t i=0; i < m_set.size(); ++i ) count += this->count(i); 

		return count;
	}

	inline bool empty() const 
	{ 
		for( size_t i=0; i < m_set.size(); ++i ) 
			if(m_set[i]) return false;

		return true;
	}

	inline void swap( MySet &other )
	{
		std::swap(m_set,other.m_set);
		std::swap(m_size,other.m_size);
	}

	inline MySet& set( ssize_t idx, bool val = true ) 
	{
		assert( idx < m_size );

		m_set[vec_index(idx)] |= mask(idx);
		return *this;
	}

	inline MySet& reset()
	{
		for( size_t i=0; i < m_set.size(); ++i ) 
			m_set[i] = block_t(0);

		return *this;
	}

	inline MySet& reset( size_t idx )
	{
		assert( idx < m_size );

		m_set[vec_index(idx)] &= ~(mask(idx));
		return *this;
	}

	inline bool test( size_t idx ) const
	{ 
		assert( idx < m_size );
		
		return (m_set[vec_index(idx)] & mask(idx)) != 0;
	}

	inline MySet& setUnion( const MySet& other )
	{
		assert( this->size() == other.size() );

		for( size_t i=0;  i < m_set.size(); ++i ) m_set[i] |= other.m_set[i];
		return *this;
	}

	inline MySet& setIntersection( const MySet& other )
	{
		assert( this->size() == other.size() );

		for( size_t i=0;  i < m_set.size(); ++i ) m_set[i] &= other.m_set[i];
		return *this;
	}

	inline MySet& rightShift( size_t shift )
	{
		if( shift >= m_size ) return this->reset();

		assert( m_set.size() > 0 );

		block_t *data = m_set.data(); 

		size_t pos = shift / bits_per_block;
		size_t off = shift % bits_per_block;
		size_t last = m_set.size() - pos - 1;
		size_t next_off = bits_per_block-off;
		
		if( off == 0 ) // shift is a multiple of bits_per_block
		{
			for( size_t i = 0; i<=last; ++i ) 
				data[i] = data[i+pos];
		}
		else
		{
			for( size_t i=0; i < last; ++i ) 
				data[i] = (data[i+pos] >> off) | (data[i+pos+1] << next_off);

			data[last] = data[m_set.size()-1] >> off;
		}

		std::fill_n(data+last+1,pos,block_t(0));

		return *this;
	}

	inline MySet& leftShift( size_t shift )
	{
		if (shift >= m_size) return this->reset();

		assert( m_set.size() > 0 );

		block_t *data = m_set.data(); 

		size_t last = m_set.size() - 1;
		size_t pos = shift / bits_per_block;
		size_t off = shift % bits_per_block;
		size_t prev_off = bits_per_block-off;
		
		if( off == 0 ) // shift is a multiple of bits_per_block
		{
			for( size_t i = last; i>=pos; --i ) 
				data[i] = data[i-pos];
		}
		else
		{
			for( size_t i=last; i > pos; --i ) 
				data[i] = (data[i-pos] << off) | (data[i-pos-1] >> prev_off);

			data[pos] = data[0] << off;
		}

		std::fill_n(data,pos,block_t(0));

		return *this;
	}

	inline bool equals( const MySet& other ) const
	{
		if( this->size() != other.size() ) return false;

		// check content
		for( size_t i=0; i < m_set.size(); ++i )
			if( m_set[i] != other.m_set[i] ) return false;

		return true;
	}

	// operators overloading

	inline MySet& operator|=( const MySet& other )
	{
		return this->setUnion(other);
	}

	inline MySet& operator&=( const MySet& other )
	{
		return this->setIntersection(other);
	}

	inline MySet& operator<<=( size_t i )
	{
		return this->leftShift(i);
	}	

	inline MySet& operator>>=( size_t i )
	{
		return this->rightShift(i);
	}

	inline bool operator==( const MySet &other ) const
	{
		return this->equals(other);
	}

};

#endif //MY_SET_HPP
