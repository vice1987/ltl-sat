#ifndef MY_STACK_HPP
#define MY_STACK_HPP

#include <vector>
#include <cassert>

#include "Frame.hpp"

class MyStack
{

private:

	std::vector< Frame > m_stack;

public:

	MyStack() { }
	MyStack( const MyStack& ) = delete;
	MyStack& operator=( const MyStack& ) = delete;

	inline void clear() 
	{ 
		m_stack.clear(); 
	}
	
	inline bool empty() const 
	{ 
		return m_stack.empty(); 
	}

	inline size_t size() const 
	{ 
		return m_stack.size(); 
	}

	inline void push( Frame &&frame )
	{
		m_stack.push_back(frame); 
	}

	inline void push( const Frame &frame )
	{
		m_stack.push_back(frame); 
	}

	inline void pop()
	{
		m_stack.pop_back();
	}

	inline Frame& operator[]( size_t idx )
	{
		assert( idx < size() );
		return m_stack[idx];
	}

	inline const Frame& top() const 
	{ 
		assert( size() > 0 );
		return m_stack[size()-1];
	}

	inline Frame& top() 
	{ 
		assert( size() > 0 );
		return m_stack[size()-1];
	}

};

#endif //MY_STACK_HPP
